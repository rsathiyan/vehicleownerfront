import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { FormattedMessage } from 'umi-plugin-react/locale';
import { Card, Typography, Alert } from 'antd';
import styles from './Welcome.less';

// const CodePreview = ({ children }) => (
//   <pre className={styles.pre}>
//     <code>
//       <Typography.Text copyable>{children}</Typography.Text>
//     </code>
//   </pre>
// );

export default () => (
  <PageHeaderWrapper>
    <Card>
      <div>
        <h1 className={styles.heading}>WELCOME TO GARAGE APP</h1>
        <p>Now you logged into the account and now you can add your vehicle and book garage</p>
        <h3>Enjoy</h3>
      </div>



    </Card>
  </PageHeaderWrapper>
);


