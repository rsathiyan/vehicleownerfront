import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { FormattedMessage } from 'umi-plugin-react/locale';
import { Card, Typography, Alert } from 'antd';
import styles from "../Vehicle/vehicle.less";

import { Table, Input, InputNumber, Popconfirm, Form } from 'antd';
import {connect} from "react-redux";


const data =[];

const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;


    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `Please Input ${title}!`,
                },
              ],
              initialValue: record[dataIndex],
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {data, editingKey: '' };
    this.columns = [
      {
        title: 'ID',
        dataIndex: 'id',
        width: '10%',
        editable: false,
      },
      {
        title: 'Vehicle Name',
        dataIndex: 'vehiclename',
        width: '35%',
        editable: true,
      },
      {
        title: 'Vehicle Number',
        dataIndex: 'vehiclenumber',
        width: '35%',
        editable: true,
      },

      {
        title: 'Book Garage',
        dataIndex: 'operation',
        render: (text, record) => {
          const { editingKey } = this.state;


          const editable = this.isEditing(record);

          return editable ? (

            <span>


              <Popconfirm title="Sure to Book?" onConfirm={() => this.book(record)}>
                <a>Book </a>
              </Popconfirm>

            </span>
          ) : (
            <a disabled={editingKey !== ''} onClick={() => this.edit(record.id)}>
              Book
            </a>

          );
        },
      },
    ];
  }

  isEditing = record => record.id === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  book ( key) {
    console.log(key);


    const {dispatch} = this.props;
    dispatch({
      type: 'viewvehicle/bookvehicle',
      payload: {   vehiclenumber: key.vehiclenumber    },
    });

  };

  save(form, key) {
    form.validateFields((error, row) => {

      console.log(row);

      if (error) {
        return;
      }

      console.log("Mallllll");
      const newData = [...this.state.data];



      const index = newData.findIndex(item => key === item.key);

      console.log(index);


      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        this.setState({ data: newData, editingKey: '' });
      } else {
        newData.push(row);
        console.log("else");
        console.log(row);
        this.setState({ data: newData, editingKey: '' });
      }
      console.log('Received values of form: ', newData);
      const {dispatch} = this.props;
      dispatch({
        type: 'viewvehicle/updatevehicle',
        payload: { ...newData },
      });


    });
  }

  edit(key) {
    this.setState({ editingKey: key });
  }

  componentDidMount() {
    console.log("componentDidMount")
    const {dispatch} = this.props;
    dispatch({
      type: 'viewvehicle/fetchvehicle',
      //dataarr.push(data)
    });
    // data=((this.props.viewvehicle.status));
  }

  render() {
    const components = {
      body: {
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === 'age' ? 'number' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return (
      <PageHeaderWrapper>
        <Card>

          <EditableContext.Provider value={this.props.form}>
            <Table
              components={components}
              bordered
              // dataSource={this.state.data}
              dataSource={this.props.viewvehicle.status}
              columns={columns}
              rowClassName="editable-row"
              pagination={{
                onChange: this.cancel,
              }}
            />
          </EditableContext.Provider>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

const EditableFormTable = Form.create()(EditableTable);

// ReactDOM.render(<EditableFormTable />, mountNode);

export default connect(({viewvehicle,loading }) => ({
  viewvehicle : viewvehicle,
  submitting: loading.effects['viewvehicle/viewvehicle'],
}))(EditableFormTable);
