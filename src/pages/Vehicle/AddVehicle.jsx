import { Form, Icon, Input, Button, Checkbox } from 'antd';
import {connect} from "react-redux";

class AddVehicleForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        const { dispatch } = this.props;
        dispatch({
          type: 'viewvehicle/addvehicle',
          payload: { ...values },
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <Form.Item>
          {getFieldDecorator('vehiclename', {
            rules: [{ required: true, message: 'Please input your vehicle name!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Vehicle Name"
            />,
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('vehiclenumber', {
            rules: [{ required: true, message: 'Please input your vehicle number!' }],
          })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Vehicle Number"
            />,
          )}
        </Form.Item>
        <Form.Item>

          <Button type="primary" htmlType="submit" className="login-form-button">
            Add Vehicle
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const WrappedAddVehicleForm = Form.create({ name: 'normal_login' })(AddVehicleForm);

export default connect(({viewvehicle,loading }) => ({
  viewvehicle : viewvehicle,
  submitting: loading.effects['viewvehicle/viewvehicle'],
}))(WrappedAddVehicleForm);
