import React from 'react';
import { PageHeaderWrapper } from '@ant-design/pro-layout';
import { FormattedMessage } from 'umi-plugin-react/locale';
import { Card, Typography, Alert } from 'antd';


export default () => (
  <PageHeaderWrapper>
    <Card>

    </Card>
    <p
      style={{
        textAlign: 'center',
        marginTop: 24,
      }}
    >
      You can create only one user with particular username{' '}
      <a href="/register/" target="_blank" rel="noopener noreferrer">
        Register
      </a>
      。
    </p>
  </PageHeaderWrapper>
);
