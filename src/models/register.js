import { stringify } from 'querystring';
import router from 'umi/router';
import { fakeAccountLogin, fakeAccountRegister } from '@/services/login';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import {getAuthority} from "../utils/authority";
const Model = {
  namespace: 'register',
  state: {
    status: undefined,
  },
  effects: {
    *register({ payload }, { call, put }) {

      console.log(payload);

      const response = yield call(fakeAccountRegister, payload);
      yield put({
        type: 'changeLoginStatus',
        payload: response,
      }); // Login successfully

      console.log(payload);

      if (response.status === 'ok') {

        console.log("successfully created");

        const urlParams = new URL(window.location.href);

        const params = getPageQuery();

        console.log(params);
        let { redirect } = params;

        if (redirect) {

          const redirectUrlParams = new URL(redirect);

          if (redirectUrlParams.origin === urlParams.origin) {
            redirect = redirect.substr(urlParams.origin.length);

            if (redirect.match(/^\/.*#/)) {
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }

        router.replace(redirect || '/garage/home/welcome');
      }
      else if (response.status === 'USER ALREADY EXIST') {
        console.log(response.status);

      }
    },

    // *getCaptcha({ payload }, { call }) {
    //   yield call(getFakeCaptcha, payload);
    // },

    logout() {
      const { redirect } = getPageQuery(); // Note: There may be security issues, please note
      if (window.location.pathname !== '/login' && !redirect) {
        router.replace({
          pathname: '/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {


      setAuthority(payload.token);
      // setAuthority(payload.currentAuthority);
      return { ...state, status:payload.status, token:payload.token};
      // return { ...state, status: payload.status, type: payload.type };
    },
  },
};
export default Model;
