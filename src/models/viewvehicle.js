import { stringify } from 'querystring';
import router from 'umi/router';
import { updateVehicle, fetchVehicle, deleteVehicle, addvehicle, bookvehicle } from '@/services/vehicle';
import { setAuthority } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import {getAuthority} from "../utils/authority";
import data from "../pages/Vehicle/viewVehicle";

const Model = {
  namespace: 'viewvehicle',

  state: {
    status: undefined,
  },

  effects: {
    *updatevehicle({ payload }, { call, put }) {

      console.log(payload[0]);

      // yield call(updateVehicle, payload[0]);

      const response = yield call(updateVehicle, payload[0]);
      yield put({
        type: 'changeupdateVehicle',
        payload: response,
      }); // Login successfully

      console.log(response);

    },

    *fetchvehicle({payload}, { call, put }) {


      // yield call(updateVehicle, payload[0]);
      console.log("payload = " +payload);

      const response = yield call(fetchVehicle, payload);
      yield put({
        type: 'changefetchVehicle',
        payload: {status : response},
      }); // Login successfully
      // data.push(response)

    },

    *deletevehicle({payload}, { call, put }) {


      // yield call(deleteVehicle, payload[0]);
      console.log(payload);

      const response = yield call(deleteVehicle, payload);
      yield put({
        type: 'changedeleteVehicle',
        payload: response,
      }); // Login successfully
      // data.push(response)
      console.log(response);

    },

    *addvehicle({payload}, { call, put }) {


      // yield call(deleteVehicle, payload[0]);
      console.log(payload);

      const response = yield call(addvehicle, payload);
      yield put({
        type: 'changeaddvehicle',
        payload: response,
      }); // Login successfully
      // data.push(response)
      console.log(response);

    },

    *bookvehicle({payload}, { call, put }) {


      // yield call(deleteVehicle, payload[0]);
      console.log(payload);

      const response = yield call(bookvehicle, payload);
      yield put({
        type: 'changebookvehicle',
        payload: response,
      }); // Login successfully
      // data.push(response)
      console.log(response);

    },


  },
  reducers: {

    changeupdateVehicle(state, { payload }) {

      // viewVehicle.setData(payload);

      return { ...state};
    },
    changefetchVehicle(state, { payload }) {

      // viewVehicle.setData(payload);

      return { ...state, ...payload};
    },
    changedeleteVehicle(state, { payload }) {

      // viewVehicle.setData(payload);

      return { ...state, ...payload};
    },
    changeaddvehicle(state, { payload }) {

      // viewVehicle.setData(payload);

      return { ...state, ...payload};
    },

    changebookvehicle(state, { payload }) {

      // viewVehicle.setData(payload);

      return { ...state, ...payload};
    },
  },
};
export default Model;
