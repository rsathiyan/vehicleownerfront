import request from '@/utils/request';
import {getAuthority} from "../utils/authority";
export async function updateVehicle(params) {


  const auth= "Bearer "+ getAuthority().toString();


  return request('/api/updateVehicle', {
    method: 'POST',
    data: params,
    headers: {
      Authorization: auth,
    }
  });
}


export async function fetchVehicle() {


  const auth= "Bearer "+ getAuthority().toString();


  return request('/api/fetchVehicle', {
    method: 'POST',
    headers: {
      Authorization: auth,
    }
  });
}

export async function deleteVehicle(params) {


  const auth= "Bearer "+ getAuthority().toString();


  return request('/api/deleteVehicle', {
    method: 'POST',
    data: params,
    headers: {
      Authorization: auth,
    }
  });
}

export async function addvehicle(params) {


  const auth= "Bearer "+ getAuthority().toString();


  return request('/api/addVehicle', {
    method: 'POST',
    data: params,
    headers: {
      Authorization: auth,
    }
  });
}

export async function bookvehicle(params) {


  const auth= "Bearer "+ getAuthority().toString();


  return request('/api/bookvehicle', {
    method: 'POST',
    data: params,
    headers: {
      Authorization: auth,
    }
  });
}


