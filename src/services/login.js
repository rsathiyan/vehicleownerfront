import request from '@/utils/request';
export async function fakeAccountLogin(params) {

  return request('/api/authenticated', {
    method: 'POST',
    data: params,
  });
}
// export async function fakeAccountRegister(params) {
//   return request(`/api/create?${params}`);
// }
export async function fakeAccountRegister(params) {
  return request('/api/create', {
    method: 'POST',
    data: params,
  });
}
